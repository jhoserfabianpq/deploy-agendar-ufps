--
-- Database: `agendar`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id_appointment` int NOT NULL COMMENT 'Identificador único de la cita.',
  `name` varchar(255) NOT NULL NULL COMMENT 'Nombre de la cita.',
  `url_meet` varchar(45) CHARACTER SET utf8mb4  DEFAULT NULL  COMMENT 'URL de la reunión de Google Meet.',
  `location` varchar(255) NOT NULL COMMENT 'Ubicacion de la cita.',
  `event_id` int NOT NULL COMMENT 'Identificador único del evento asociado.',
  `comments` text CHARACTER SET utf8mb4  COMMENT 'Comentarios adicionales sobre la cita.',
  `period` varchar(6) DEFAULT NULL COMMENT 'Periodo respestivo del año. (año+semestre).',
  `appointment_start` datetime DEFAULT NULL COMMENT 'Fecha y hora de inicio de la cita.',
  `appointment_end` datetime DEFAULT NULL COMMENT 'Fecha y hora de fin de la cita.',
  `created_at` datetime DEFAULT NULL COMMENT 'Fecha y hora de creación de la cita.',
  `id_google_calendar` varchar(255) DEFAULT NULL COMMENT 'Identificador único de Google Calendar.',
  `link_google_calendar` varchar(255) DEFAULT NULL COMMENT 'Enlace a Google Calendar.',
  `topic_id` int DEFAULT NULL COMMENT 'Identificador único del tema asociado.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `appointment` COMMENT = 'Tabla para almacenar las citas del usuario.';
-- --------------------------------------------------------

--
-- Table structure for table `availability`
--

CREATE TABLE `availability` (
  `id_availability` int NOT NULL COMMENT 'Identificador único de la disponibilidad.',
  `name` varchar(45) DEFAULT NULL COMMENT 'Nombre de la disponibilidad.',
  `created_by` int DEFAULT NULL COMMENT 'Identificador del usuario creador de la disponibilidad.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `availability` COMMENT = 'Tabla para almacenar la disponibilidad de eventos.';
-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id_event` int NOT NULL COMMENT 'Identificador único del evento.',
  `description` text COMMENT 'Descripción del evento.',
  `name` varchar(45) NOT NULL COMMENT 'Nombre del evento.',
  `duration` smallint NOT NULL DEFAULT '15' COMMENT 'Duración del evento.',
  `location` varchar(255) CHARACTER SET utf8mb4  NOT NULL COMMENT 'Ubicación del evento. Codigo de salón o "Meet"',
  `type_meeting` varchar(255) NOT NULL  COMMENT 'Tipo de evento. Mixto, Meet o presencial.',
  `state` tinyint DEFAULT '1' COMMENT 'Estado del evento. Publico o Privado',
  `user_id` int NOT NULL COMMENT 'Identificador único del usuario asociado al evento.',
  `created_at` datetime DEFAULT NULL COMMENT 'Fecha y hora de creación del evento.',
  `updated_at` datetime DEFAULT NULL COMMENT 'Fecha y hora de última actualización del evento.',
  `period` varchar(6) DEFAULT NULL COMMENT 'Periodo respestivo del año. (año+semestre).',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'Propiedad que indica si fue eliminado.',
  `availability_id` int DEFAULT NULL COMMENT 'Identificador de la disponibilidad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `event` COMMENT = 'Tabla para almacenar información sobre eventos.';
-- --------------------------------------------------------

--
-- Table structure for table `historic`
--

CREATE TABLE `historic` (
  `id_historic` int NOT NULL COMMENT 'Identificador único del registro histórico.',
  `email_organizer` varchar(45) DEFAULT NULL  COMMENT 'Correo electrónico del organizador del evento.',
  `event_name` varchar(45) DEFAULT NULL COMMENT 'Nombre del evento.',
  `topics` longtext COMMENT 'Temas relacionados con el evento.',
  `description` varchar(255) NOT NULL COMMENT 'Descripción del evento.',
  `attendees` longtext COMMENT 'Lista de asistentes al evento.',
  `created_at` datetime DEFAULT NULL COMMENT 'Fecha y hora de creación del registro histórico.',
  `duration` int DEFAULT NULL COMMENT 'Duración del evento.',
  `date_end` datetime DEFAULT NULL  COMMENT 'Fecha y hora de finalización del evento.',
  `period` varchar(6) CHARACTER SET utf8mb4  DEFAULT NULL COMMENT 'Período del evento.',
  `date_start` datetime DEFAULT NULL COMMENT 'Fecha y hora de inicio del evento.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `historic` COMMENT = 'Tabla para almacenar información histórica de citas.';
-- --------------------------------------------------------

--
-- Table structure for table `intervals`
--

CREATE TABLE `intervals` (
  `id_intervals` int NOT NULL COMMENT 'Identificador único del intervalo de tiempo.',
  `date_start` time DEFAULT NULL COMMENT 'Fecha y hora de inicio del intervalo.',
  `date_end` time DEFAULT NULL COMMENT 'Fecha y hora de fin del intervalo.',
  `week_day_id` int NOT NULL COMMENT 'Identificador único del día de la semana asociado al intervalo.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `intervals` COMMENT = 'Tabla para almacenar los intervalos de tiempo asociados a los días de la semana.';
-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id_topic` int NOT NULL COMMENT 'Identificador único del tema.',
  `name` varchar(100) DEFAULT NULL COMMENT 'Nombre del tema.',
  `created_at` datetime DEFAULT NULL COMMENT 'Fecha y hora de creación del tema.',
  `updated_at` datetime DEFAULT NULL COMMENT 'Fecha y hora de última actualización del tema.',
  `version` int DEFAULT NULL COMMENT 'Versión del tema.',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'Propiedad que indica si fue eliminado.',
  `created_by` int DEFAULT NULL COMMENT 'Identificador del usuario creador del tema.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `topics` COMMENT = 'Tabla para almacenar los temas relacionados con los eventos.';
-- --------------------------------------------------------

--
-- Table structure for table `topics_event`
--

CREATE TABLE `topics_event` (
  `topics_id` int NOT NULL COMMENT 'Identificador único del tema asociado.',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT 'Propiedad que indica si fue eliminado.',
  `event_id` int NOT NULL COMMENT 'Identificador único del evento asociado.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `topics_event` COMMENT = 'Tabla de relación entre temas y eventos.';
-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int NOT NULL COMMENT 'Identificador único del usuario.',
  `email` varchar(70) NOT NULL COMMENT 'Correo electrónico del usuario.', 
  `first_name` varchar(50) DEFAULT NULL COMMENT 'Nombres del usuario.',
  `last_name` varchar(50) DEFAULT NULL  COMMENT 'Apellidos del usuario.',
  `picture_url` varchar(255) DEFAULT NULL COMMENT 'URL de la imagen del usuario.',
  `created_at` datetime NOT NULL COMMENT 'Fecha y hora de creación del usuario.',
  `updated_at` datetime NOT NULL COMMENT 'Fecha y hora de última actualización del usuario.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `user` COMMENT = 'Tabla para almacenar información de los usuarios.';
-- --------------------------------------------------------

--
-- Table structure for table `user_appointment`
--

CREATE TABLE `user_appointment` (
  `id_user` int NOT NULL COMMENT 'Identificador único del usuario asociado.',
  `id_appointment` int NOT NULL COMMENT 'Identificador único de la cita asociada.',
  `attended` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Asistencia del invitado. 1 Asistió y 0 No Asistió'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `user_appointment` COMMENT = 'Tabla de relación entre usuarios y citas.';
-- --------------------------------------------------------

--
-- Table structure for table `week_day`
--

CREATE TABLE `week_day` (
  `id_week` int NOT NULL  COMMENT 'Identificador único del día de la semana.',
  `name` varchar(45) CHARACTER SET utf8mb4  NOT NULL COMMENT 'Nombre del día de la semana.',
  `availability_id` int NOT NULL COMMENT 'Identificador único de la disponibilidad asociada al día de la semana.',
  `state` tinyint NOT NULL DEFAULT '1' COMMENT 'Estado del día de la semana (activo/inactivo).'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
ALTER TABLE `week_day` COMMENT = 'Tabla para almacenar los días de la semana asociados a la disponibilidad.';
--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id_appointment`),
  ADD KEY `fk_appointment_event1_idx` (`event_id`),
  ADD KEY `fk_topic_id` (`topic_id`);

--
-- Indexes for table `availability`
--
ALTER TABLE `availability`
  ADD PRIMARY KEY (`id_availability`),
  ADD KEY `fk_availability_user` (`created_by`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `fk_event_user1_idx` (`user_id`),
  ADD KEY `fk_event_availability` (`availability_id`);

--
-- Indexes for table `historic`
--
ALTER TABLE `historic`
  ADD PRIMARY KEY (`id_historic`);

--
-- Indexes for table `intervals`
--
ALTER TABLE `intervals`
  ADD PRIMARY KEY (`id_intervals`),
  ADD KEY `fk_intervals_week_day1_idx` (`week_day_id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id_topic`),
  ADD KEY `fk_topic_created_by` (`created_by`);

--
-- Indexes for table `topics_event`
--
ALTER TABLE `topics_event`
  ADD PRIMARY KEY (`topics_id`,`event_id`),
  ADD KEY `fk_topics_has_event_event1_idx` (`event_id`),
  ADD KEY `fk_topics_has_event_topics1_idx` (`topics_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Indexes for table `user_appointment`
--
ALTER TABLE `user_appointment`
  ADD PRIMARY KEY (`id_user`,`id_appointment`),
  ADD KEY `fk_user_has_appointment_appointment1_idx` (`id_appointment`),
  ADD KEY `fk_user_has_appointment_user1_idx` (`id_user`);

--
-- Indexes for table `week_day`
--
ALTER TABLE `week_day`
  ADD PRIMARY KEY (`id_week`),
  ADD KEY `fk_week_day_availability1_idx` (`availability_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id_appointment` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `availability`
--
ALTER TABLE `availability`
  MODIFY `id_availability` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id_event` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `historic`
--
ALTER TABLE `historic`
  MODIFY `id_historic` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `intervals`
--
ALTER TABLE `intervals`
  MODIFY `id_intervals` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id_topic` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `week_day`
--
ALTER TABLE `week_day`
  MODIFY `id_week` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `fk_appointment_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id_event`),
  ADD CONSTRAINT `fk_topic_id` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id_topic`);

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_event_availability` FOREIGN KEY (`availability_id`) REFERENCES `availability` (`id_availability`),
  ADD CONSTRAINT `fk_event_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `intervals`
--
ALTER TABLE `intervals`
  ADD CONSTRAINT `fk_intervals_week_day1` FOREIGN KEY (`week_day_id`) REFERENCES `week_day` (`id_week`) ON DELETE CASCADE;

--
-- Constraints for table `topics`
--
ALTER TABLE `topics`
  ADD CONSTRAINT `fk_topic_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `topics_event`
--
ALTER TABLE `topics_event`
  ADD CONSTRAINT `fk_topics_has_event_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id_event`);

--
-- Constraints for table `user_appointment`
--
ALTER TABLE `user_appointment`
  ADD CONSTRAINT `fk_user_has_appointment_appointment1` FOREIGN KEY (`id_appointment`) REFERENCES `appointment` (`id_appointment`),
  ADD CONSTRAINT `fk_user_has_appointment_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `week_day`
--
ALTER TABLE `week_day`
  ADD CONSTRAINT `fk_week_day_availability1` FOREIGN KEY (`availability_id`) REFERENCES `availability` (`id_availability`);
  
  
DELIMITER $$
CREATE PROCEDURE `updateEvents`()
BEGIN
    DECLARE currentMonth INT;
    DECLARE nextPeriod VARCHAR(6);
    DECLARE currentPeriod VARCHAR(6);
    DECLARE updatedCount INT;
    DECLARE curDateTime DATETIME;
    DECLARE tempName VARCHAR(45);

    -- Obtener el mes actual
    SET currentMonth = MONTH(CURDATE());
    SET curDateTime = NOW(); 
    
    -- Verificar si estamos antes o después del 30 de junio para determinar el periodo
    IF currentMonth = 6 AND DAY(CURDATE()) = 30 THEN
        -- Si estamos antes o en junio, el periodo será el mismo año más el mes 01
        SET nextPeriod = CONCAT(YEAR(CURDATE()), '02');
        SET currentPeriod = CONCAT(YEAR(CURDATE()), '01');
    END IF;
    IF currentMonth = 12 AND DAY(CURDATE()) = 30 THEN
        -- El periodo será el siguiente año más el mes 01
        SET nextPeriod = CONCAT(YEAR(CURDATE()) + 1, '01');
        SET currentPeriod = CONCAT(YEAR(CURDATE()), '02');
    END IF;
    
	/*EVENT*/
    -- Marcar como eliminados los eventos activos que no estén ya eliminados en el periodo actual
    UPDATE event 
    SET is_deleted = 1,
         availability_id = null,
         name = CONCAT(name, '_', currentPeriod)
    WHERE is_deleted = 0
    AND created_at < curDateTime;
    
    /*TOPIC*/
    -- Marcar como eliminados los topics activos que no estén ya eliminados en el periodo actual
    UPDATE topics
    SET is_deleted = 1,
         name = CONCAT(name, '_', currentPeriod)
    WHERE is_deleted = 0
    AND created_at < curDateTime;
END$$
DELIMITER ;
COMMIT;


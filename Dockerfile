# Dockerfile para servicio backend

# Definir una imagen base
FROM openjdk:19

# Establecer el directorio de trabajo en el contenedor
WORKDIR /code

# Copiar todos los archivos del directorio actual al contenedor
COPY ./backend/tarjet/agendarufps.jar/ /code/

# Exponer el puerto 8080 para que la aplicación esté disponible desde fuera del contenedor
EXPOSE 8080

# Definir argumentos de construcción para las variables de entorno
ARG DB_USER
ARG DB_PASSWORD
ARG DB_URL
ARG BASE_URL
ARG FRONTEND_HOST
ARG GOOGLE_CLIENT_ID
ARG GOOGLE_CLIENT_SECRET
ARG GOOGLE_CLIENT_NAME
ARG GOOGLE_REDIRECT_URI


# Establecer las variables de entorno en el contenedor
ENV DB_USER=$DB_USER
ENV DB_PASSWORD=$DB_PASSWORD
ENV DB_URL=$DB_URL
ENV BASE_URL=$BASE_URL
ENV FRONTEND_HOST=$FRONTEND_HOST
ENV GOOGLE_CLIENT_ID=$GOOGLE_CLIENT_ID
ENV GOOGLE_CLIENT_SECRET=$GOOGLE_CLIENT_SECRET
ENV GOOGLE_CLIENT_NAME=$GOOGLE_CLIENT_NAME
ENV GOOGLE_REDIRECT_URI=$GOOGLE_REDIRECT_URI

# Comando para ejecutar la aplicación Spring Boot al iniciar el contenedor
CMD ["java", "-jar", "/code/agendarufps.jar"]
<div align="center">
  <img src="https://gitlab.com/ronaldeduardobm/agendarufps/-/raw/main/src/assets/images/logo-white.svg" alt="Logo AGENDAR">
</div>

## Descripción
El proyecto propone desarrollar una plataforma accesible y fácil de usar que integre las cuentas institucionales de Google con el servicio de Google Calendar para facilitar la programación y gestión de asesorías entre profesores y estudiantes en la Universidad Francisco de Paula Santander (UFPS). Este sistema permitirá automatizar la reserva de citas, reduciendo así los errores humanos y las ineficiencias asociadas con los métodos manuales.

## Tecnologías
* Docker

### Requisitos previos
* Tecnologías anteriormente mencionadas
* Tener conocimientos básicos de Docker
* Tener un proyecto de Google Cloud Platform con las credenciales de OAuth 2.0
* Configurar las siguientes variables de entorno en un archivo env
```
DB_USER=usuario de base de datos
DB_PASSWORD=contraseña de la base de datos
DB_URL=Url de la base de datos

FRONTEND_HOST=URL Front por cors 

GOOGLE_CLIENT_ID=Id del cliente generado por las credenciales de google
GOOGLE_CLIENT_SECRET=Secreto del cliente generado por las credenciales de google
GOOGLE_CLIENT_NAME=Nombre del proyecto de google
GOOGLE_REDIRECT_URI=URI de redireccionamiento creado en las credenciales de google
```
## Estructura del proyecto
```bash
│ 
├─ backend.tarjet/
│       └─ agendarufps.jar
│
├───── db/
│       └─ DDL Agendar.sql
│
├─ frontend/
│       ├─ dist.agendar-ufps/
│       │   └─ browser/
│       │       ├─ assets/  
│       │       └─ index.html
│       │
│       └─ nginx-frontend.conf
│
└─ docker-compose.yml
```
**backend/tajet/**:  Directorio que contiene el archivo JAR del backend del proyecto.

**db/**: Contiene los archivos necesarios relacionado con la base de datos. Archivo SQL con las instrucciones de creación de tablas y definición de datos para la base de datos del proyecto.

**frontend/**: Directorio que contiene los archivos del frontend del proyecto.

- **nginx-frontend.conf**: Archivo de configuración de NGINX para el frontend.

**docker-compose.yml**: Archivo de configuración de Docker Compose para orquestar los contenedores del proyecto.
## Ejecución del proyecto
```
git clone https://gitlab.com/jhoserfabianpq/deploy-agendar-ufps.git
cd deploy-agendar-ufps
 ```
Ejecutamos el comando para construir y levantar los contenedores
```
docker-compose --env-file sample.env up -d --build
```


## Desarrollado Por:

- **Jhoser Fabian Pacheco Quintero** - Código: 1151807
    - **Correo**: [jhoserfabianpq@ufps.edu.co](mailto:jhoserfabianpq@ufps.edu.co)

- **Ronald Eduardo Benitez Mejia** - Código: 1151813
    - **Correo**: [ronaldeduardobm@ufps.edu.co](mailto:ronaldeduardobm@ufps.edu.co)

## Dirección del Proyecto

- **Director**: PhD. Marco Antonio Adarme Jaimes
- **Co-Directora**: PhD. Judith del Pilar Rodríguez Tenjo


___

<div style="display: flex;">
  <img src="https://gitlab.com/ronaldeduardobm/agendarufps/-/raw/main/src/assets/images/ufps.png" alt="Logos institucionales">
</div> 

